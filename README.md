# SBKOP

Suit Bots Kit of Parts

## Overview

The Suit Bots Kit of Parts is a library of components that we've designed
to help us build FTC robots. They are mostly designed to accommodate our
building style of using 8020 1010-profile extrusion for the chassis, VexPro
drivetrain components, Rev 15mm extrusion and various Actobotics motion for
the rest of the robot.

## Available parts for 10-series 80/20 extrusion

* SB0001 ([stl](parts/SB0001.stl), [step](parts/SB0001.step)) NeveRest Classic motor mount
* SB0002 ([stl](parts/SB0002.stl), [step](parts/SB0002.step)) NeveRest Orbital motor mount
* SB0003 ([stl](parts/SB0003.stl), [step](parts/SB0003.step)) Rev Core Hex motor mount
* SB0004 ([stl](parts/SB0004.stl), [step](parts/SB0004.step)) NeveRest Classic motor mount V2
* SB0005 ([stl](parts/SB0005.stl), [step](parts/SB0005.step)) Versa planetary motor mount
* SB0006 ([stl](parts/SB0006.stl), [step](parts/SB0006.step)) Attach a 9mm bearing bore hole to 10-series 80/20 ([Example](https://a360.co/2nvRlYw))
* SB0007 ([stl](parts/SB0007.stl), [step](parts/SB0007.step)) Attach a 9mm bearing bore hole to the end of a 10-series 80/20 extrusion
* SB0008 ([stl](parts/SB0008.stl), [step](parts/SB0008.step)) 1/4-20 loose to M3-close hole adapter ([Example](https://a360.co/2ntQAyY) joining 80/20 1080 extrusion at a 90 degree angle to Rev 15mm extrusion)
* SB0009 ([stl](parts/SB0009.stl), [step](parts/SB0009.step)) Side bearing mount, 2.1" drop for 1.125" bearing bore
* SB0010 ([stl](parts/SB0010.stl), [step](parts/SB0010.step)) Side bearing mount, 2.2" drop for 1.125" bearing bore

